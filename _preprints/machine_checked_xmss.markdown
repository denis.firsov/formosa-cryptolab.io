---
layout: default
title: "Machine-Checked Security for XMSS
as in RFC 8391 and SPHINCS<sup>+</sup>"
year: 2023
authors: Manuel Barbosa, François Dupressoir, Benjamin Grégoire, Andreas Hülsing, Matthias Meijers, Pierre-Yves Strub
website: https://eprint.iacr.org/2023/408
conference: IACR, 2023

---