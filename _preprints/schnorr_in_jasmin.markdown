---
layout: default
title: "Schnorr Protocol in Jasmin"
year: 2023
authors: Josè Bacelar Almeida, Denis Firsov, Tiago Oliveira, and Dominique Unruh
website: https://firsov.ee/schnorr-in-jasmin/
conference: IACR, 2023
---
