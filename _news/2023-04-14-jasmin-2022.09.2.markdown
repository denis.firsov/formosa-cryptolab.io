---
layout: default

title: Jasmin release 2022.09.2
date: 2023-04-14

summary: >-
  A new minor version of Jasmin is available.
linkback: >-
  Read the announcement.
---

# Release 2022.09.2 of the Jasmin compiler

A new minor version of the Jasmin compiler has just been released.
It brings in the support of new x86 instructions, of new compound assignments
(modulo and division), and, most importantly, many fixes, especially to the
safety checker.
More details can be found in the
[CHANGELOG](https://github.com/jasmin-lang/jasmin/blob/v2022.09.2/CHANGELOG.md#jasmin-2022092).

