---
layout: page
title: News

permalink: /news/
---

{% for item in site.news %}
- [**{{ item.title }}**]({{ item.url }})<br/>
  {{ item.summary | markdownify }}
{% endfor %}
