---
layout: default
title: Enforcing fine-grained constant-time policies
year: 2022
authors: Basavesh Ammanaghatta Shivakumar, Gilles Barthe, Benjamin Grégoire, Vincent Laporte, and Swarn Priya
website: https://eprint.iacr.org/2022/630
conference: CCS, 2022

---