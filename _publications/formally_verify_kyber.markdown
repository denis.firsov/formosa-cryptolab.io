---
layout: default
title: "Formally verifying Kyber Part I: Implementation Correctness"
year: 2023
authors: José Bacelar Almeida, Manuel Barbosa, Gilles Barthe, Benjamin Grégoire, Vincent Laporte, Jean-Christophe Léchenet, Tiago Oliveira, Hugo Pacheco, Miguel Quaresma, Peter Schwabe, Antoine Séré, Pierre-Yves Strub
website: https://eprint.iacr.org/2023/215
conference: TCHES, 2023

---