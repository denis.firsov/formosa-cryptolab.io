---
layout: default
title: "Strong Non-Interference and Type-Directed Higher-Order Masking"
year: 2016
authors: Gilles Barthe, Sonia Belaïd, François Dupressoir, Pierre-Alain Fouque, Benjamin Grégoire, Pierre-Yves Strub, and Rébecca Zucchini
website: https://eprint.iacr.org/2015/506
conference: CCS, 2016

---